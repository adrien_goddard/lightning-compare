<?php

use Compare\ORM\EloquentModel\Customer;
use Compare\ORM\EloquentModel\Order;
use Illuminate\Database\Capsule\Manager;

require __DIR__ . '/../../vendor/autoload.php';

$capsule = new Manager;
$capsule->addConnection([
	'driver' => 'mysql',
	'host' => 'localhost',
	'database' => 'lightning-compare_eloquent',
	'username' => 'root',
	'password' => '',
	'charset' => 'utf8',
]);
$capsule->setAsGlobal();
$capsule->bootEloquent();

// log the queries
$capsule->getConnection()->enableQueryLog();

$bootTime = (microtime(true) - $_SERVER['REQUEST_TIME_FLOAT']) * 1000;

switch ($argv[1]) {
	case 'truncate' :
		Customer::truncate();
		Order::truncate();
		break;
	case 'write' :
		for ($i = 0; $i < 10; $i++) {
			$customer = Customer::create([
				'name' => rand(0, 100000),
			]);

			for ($j = 0; $j < 10; $j++) {
				Order::create([
					'customer_id' => $customer->id,
					'amount' => rand(0, 100000) / 100,
				]);
			}
		}
		break;
	case 'read from collection' :
		Customer::all()
				->pluck('orders')
				->collapse()
				->pluck('amount')
				->sum();
		break;
	case 'read from entity' :
		for ($i = 0; $i < 100; $i++) {
			Order::find($i);
		}
		break;
}

$time = (microtime(true) - $_SERVER['REQUEST_TIME_FLOAT']) * 1000 - $bootTime;

echo json_encode([
	'boot_time' => round($bootTime, 2),
	'test_time' => round($time, 2),
	'queries' => count($capsule->getConnection()->getQueryLog()),
]) . PHP_EOL;
