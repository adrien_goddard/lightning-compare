<?php

namespace Compare\ORM\LightningModel;

use Lightning\ORM\Entity;

/**
  * @property int  $id
  * @property string  $name
  */
class Customer extends Entity
{
	public static $table = [
		'name' => 'customers',
		'prefix' => 'cst',
		'fields' => [
			'id' => ['type' => 'mediumint', 'unsigned' => true, 'primary_key' => true],
			'name' => ['type' => 'varchar'],
		],
	];
	
	public static $relations = [
		'orders' => [
			'type' => 'one_to_many',
			'class' => Order::class,
		],
	];
}
