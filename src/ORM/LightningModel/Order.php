<?php

namespace Compare\ORM\LightningModel;

use Lightning\ORM\Entity;

/**
  * @property int  $id
  * @property int  $cst_id
  * @property float  $amount
  * @property Customer  $customer
  */
class Order extends Entity
{
	public static $table = [
		'name' => 'orders',
		'prefix' => 'rdr',
		'fields' => [
			'id' => ['type' => 'mediumint', 'unsigned' => true, 'primary_key' => true],
			'cst_id' => ['type' => 'mediumint', 'unsigned' => true],
			'amount' => ['type' => 'float', 'unsigned' => true],
		],
	];
	
	public static $relations = [
		'customer' => [
			'type' => 'many_to_one',
			'class' => Customer::class,
		],
	];
}
