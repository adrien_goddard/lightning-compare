<?php

use Compare\ORM\LightningModel\Customer;
use Compare\ORM\LightningModel\Order;
use Lightning\ORM\Database;

require __DIR__ . '/../../vendor/autoload.php';

// connects to the DB
try {
	Database::addDatabase('mysql', 'localhost', 'lightning-compare_lightning', 'root', '');
} catch (PDOException $e) {
	echo 'Failed connecting to the database : ' . $e->getMessage();
}

// log the queries
Database::startLogging();

$bootTime = (microtime(true) - $_SERVER['REQUEST_TIME_FLOAT']) * 1000;

switch ($argv[1]) {
	case 'truncate' :
		Customer::truncate();
		Order::truncate();
		break;
	case 'write' :
		for ($i = 0; $i < 10; $i++) {
			$customer = Customer::add([
				'name' => rand(0, 100000),
			]);

			for ($j = 0; $j < 10; $j++) {
				Order::add([
					'cst_id' => $customer->id,
					'amount' => rand(0, 100000) / 100,
				]);
			}
		}
		break;
	case 'read from collection' :
		Customer::selectAll()->orders->sum('amount');
		break;
	case 'read from entity' :
		for ($i = 0; $i < 100; $i++) {
			Order::load($i);
		}
		break;
}

$time = (microtime(true) - $_SERVER['REQUEST_TIME_FLOAT']) * 1000 - $bootTime;

echo json_encode([
	'boot_time' => round($bootTime, 2),
	'test_time' => round($time, 2),
	'queries' => count(Database::getLog()),
]) . PHP_EOL;
