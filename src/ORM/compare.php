<?php

$totalExecutionTime = microtime(true);

require __DIR__ . '/../../vendor/autoload.php';

$ORMs = ['lightning', 'eloquent'];
$tests = ['write', 'read from collection', 'read from entity'];

// truncate all the tables
foreach ($ORMs as $ORM) {
	shell_exec('php ' . __DIR__ . "/$ORM.php 'truncate'");
}

// run all the tests
$results = [];
foreach ($ORMs as $ORM) {
	foreach ($tests as $test) {
		// we make a break during one second before runing the tests
		sleep(1);
		
		for ($i = 0; $i < 10; $i++) {
			$result = json_decode(shell_exec('php ' . __DIR__ . "/$ORM.php '$test'"), true);
			$results['boot_times'][$ORM][] = $result['boot_time'];
			
			$results[$test][$ORM]['test_times'][] = $result['test_time'];
			$results[$test][$ORM]['queries'] = $result['queries'];
		}
	}
}

// we keep only the best times
foreach ($ORMs as $ORM) {
	sort($results['boot_times'][$ORM]);
	$bestBootTime = $results['boot_times'][$ORM][0];
	
	$results['boot_time'][$ORM] = $bestBootTime;
	
	foreach ($tests as $test) {
		$result = $results[$test][$ORM];
		
		sort($result['test_times']);
		$bestTestTime = $result['test_times'][0];
		
		$results[$test][$ORM]['test_time'] = $bestTestTime;
	}
}

// compare the tests times
foreach ($tests as $test) {
	array_multisort(array_column($results[$test], 'test_time'), $results[$test]);
	
	$fastestORM = array_keys($results[$test])[0];
	$fastestResult = $results[$test][$fastestORM];

	echo "$test / $fastestORM : $fastestResult[test_time] ms ($fastestResult[queries] queries)" . PHP_EOL;

	foreach (array_slice($results[$test], 1) as $ORM => $result) {
		$difference = round(($result['test_time'] - $fastestResult['test_time']) / $fastestResult['test_time'] * 100);
		echo "$test / $ORM : $result[test_time] ms +$difference% ($result[queries] queries)" . PHP_EOL;
	}
}

// compare the boot times
asort($results['boot_time']);
$fastestORM = array_keys($results['boot_time'])[0];
$fastestBootTime = $results['boot_time'][$fastestORM];

echo "boot / $fastestORM : $fastestBootTime ms" . PHP_EOL;

foreach (array_slice($results['boot_time'], 1) as $ORM => $bootTime) {
	$difference = round(($bootTime - $fastestBootTime) / $fastestBootTime * 100);
	echo "boot / $ORM : $bootTime ms +$difference%" . PHP_EOL;
}

echo 'Total execution time : ' . round(microtime(true) - $totalExecutionTime, 1) . ' s' . PHP_EOL;
