<?php

namespace Compare\ORM\EloquentModel;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
	protected $table = 'customers';
    protected $fillable = ['name'];
	public $timestamps = false;
	
	public function orders()
	{
		return $this->hasMany('\Compare\ORM\EloquentModel\Order');
	}
}
