<?php

namespace Compare\ORM\EloquentModel;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	protected $table = 'orders';
    protected $fillable = ['customer_id', 'amount'];
	public $timestamps = false;
	
	public function customer()
	{
		return $this->belongsTo('\Compare\ORM\EloquentModel\Customer');
	}
}
