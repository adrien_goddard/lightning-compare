# Lightning Compare

Tries to highlight the advantages of Lightning's libraries over some others.
For now, you will only find a few advantages against a lot of missing features.

## Running the comparisons

```
$ bin/init.sh  #only required the first time to create the databases
$ php src/ORM/compare.php
```

## License

The MIT License (MIT). Please see [License File](https://bitbucket.org/adrien_goddard/lightning-compare/src/master/LICENSE) for more information.
